'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "NaClBox", {
  enumerable: true,
  get: function get() {
    return _NaClBox.default;
  }
});
Object.defineProperty(exports, "NaClUtils", {
  enumerable: true,
  get: function get() {
    return _NaClUtils.default;
  }
});

var _NaClBox = _interopRequireDefault(require("./NaClBox"));

var _NaClUtils = _interopRequireDefault(require("./NaClUtils"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }